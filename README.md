# FormationXamarin2022

<div align="center">
<h1><br>[Formationn Xamarin](https://forgemia.inra.fr/xamarin/formationxamarin2022)<br> <img src="https://www.inrae.fr/themes/custom/inrae_socle/logo.svg"><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Hackathon sur le développement mobile utilisant la technologie Xamarin/MAUI., Toulouse 2022 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br><img src="https://www6.inrae.fr/var/internet6_national_dipso/storage/images/configuration-graphique/haut/dipso2/34393-3-fre-FR/DipSO_inra_logo.png"></h1>
</div>

## L’objectif de cet hackathon est double : 

1) Fédérer une communauté sur cette technologie
2) Travailler sur des modules communs (ex : Migration MAUI, charte graphique, authentification, connexion Bluetooth, ...)
Notre ambition est donc de regrouper plusieurs développeurs qui travailleront sur la même technologie afin de partager des solutions techniques pour faciliter et industrialiser les développements mobiles.

### L’organisation est prévue en 2 temps :
- 1) Une session d’initiation à Xamarin (A Toulouse du mercredi 5/10 14h au vendredi 7/10 12h)
- 2) L’hackathon (sessions plénières + ateliers) (A Sète 4 jours fin janvier 2023 ou fin mars 2023)

Des experts et des partenaires extérieurs seront invités afin d’enrichir nos compétences.
Les inscriptions pour la session d’initiation est ouverte jusqu’au 23 septembre sur le lien ci-dessous (nombre de places limité à 20 personnes) :https://forms.gle/is5wLCeYtALSJvcr7
Les repas et hébergements sont pris en charge par l’organisation (financement DIPSO). Seuls les déplacements seront à prendre en charge par vos unités.
Si vous avez des questions, contactez-nous :
Alexandre Journaux : alexandre.journaux@inrae.fr (Cati Sicpa)
Jacques Lagnel : jacques.lagnel@inrae.fr (Cati PlantBreed)


- Forum: cannal Mattermost devmobile:
https://team.forgemia.inra.fr/devmobile

- Gitlab: sur la forgeMia:
https://forgemia.inra.fr/xamarin

## Propositions de sessions de travail pour le hackathon en mars 2023:
- Migration MAUI
- Elaboration de code (Wrappers/librairies) inter-opérables et partagés:
    - Bluetooth, BLE, BdD, Bacodes, printers (zebra),
    - Mise en place de bonnes pratiques
    - gestion d'I/O (xml, json, xls, csv, pdf )
    - Charte graphique (boutons, entry, ... et optimisation pour contraste/plein/soleil )
    - Gestion des version OS.
    - Intéropérabilité avec des SI, Webservices et APIs
    - Gestion de Connection: Identification, LDAP, ...


### - [Présentation Xamarin](https://forgemia.inra.fr/xamarin/formationxamarin2022/-/blob/main/MiniFormationXamarin.pdf)

- Pour installer Visual Studio : https://visualstudio.microsoft.com/fr/vs/older-downloads/
Personnellement, j’ai la version 2019. Donc la semaine prochaine, j’utiliserai cette version pour la formation.
Par contre, pour l’hackathon de mars 2023, nous utiliserons la version 2022 (pour avoir MAUI).
Les 2 versions sont, normalement, compatibles à condition de les installer sur des répertoires différents.
Dans tous les cas, prendre la version Community (gratuite).
 
- Créer votre première Xamarin.Forms application : https://learn.microsoft.com/fr-fr/xamarin/get-started/first-app/?pivots=windows


### Docs techniques, tutos:
- Sur la Forge dga : https://forge-dga.jouy.inra.fr/projects/xamarin/wiki
- Sur le Site Microsoft Ignite : https://learn.microsoft.com/fr-fr/dotnet/csharp/ 
- Plugins Nugets: https://www.nuget.org/

***

